﻿using System;
namespace TicTacToe
{
    public class Jogo
    {
        public Jogo() 
        {
            this.tabuleiro = new string[3, 3] 
            { 
                { "_", "_", "_" }, 
                { "_", "_", "_" }, 
                { "_", "_", "_" }
            };

            // descobre o topo do array
            this.bound0 = this.tabuleiro.GetUpperBound(0);
            this.bound1 = this.tabuleiro.GetUpperBound(1);
            this.jogador = "O";
        }

        string[,] tabuleiro;
        int bound0, bound1;
        string jogador;

        private bool EmAndamento 
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Vencedor))
                {
                    return false;
				}
                
                for (int i = 0; i <= this.bound0; i++)
                {
                    for (int j = 0; j <= this.bound1; j++)
                    {
                        if (this.tabuleiro[i, j] == "_")
                        {
                            return true;
						}
					}
				}
                return false;
            }
		} 

        private string Vencedor
        {
            get 
            {

                // verifica as diagonais
                if (
                    ((this.tabuleiro[0, 0] != "_" && this.tabuleiro[1, 1] != "_" && this.tabuleiro[2, 2] != "_") &&
                    (this.tabuleiro[0, 0] == this.tabuleiro[1, 1] && this.tabuleiro[1, 1] == this.tabuleiro[2, 2])) ||
                    ((this.tabuleiro[0, 2] != "_" && this.tabuleiro[1, 1] != "_" && this.tabuleiro[2, 0] != "_") &&
                    (this.tabuleiro[0, 2] == this.tabuleiro[1, 1] && this.tabuleiro[1, 1] == this.tabuleiro[2, 0])))
                    return this.tabuleiro[1, 1];

                for (int i = 0; i <= this.bound0; i++)
                {
					// verifica linhas
                    if ((this.tabuleiro[i, 0] != "_" && this.tabuleiro[0, 1] != "_" && this.tabuleiro[0, 2] != "_") &&
                        this.tabuleiro[i, 0] == this.tabuleiro[i, 1] &&
                        this.tabuleiro[i, 1] == this.tabuleiro[i, 2])
                        return this.tabuleiro[i, 0];
				
                }

                for (int i = 0; i <= this.bound1; i++)
				{
                    // verifica colunas
                    if ((this.tabuleiro[0, i] != "_" && this.tabuleiro[1, i] != "_" && this.tabuleiro[2, i] != "_") &&
                        this.tabuleiro[0, i] == this.tabuleiro[1, i] &&
                        this.tabuleiro[1, i] == this.tabuleiro[2, i])
                        return this.tabuleiro[0, i];
				}

                return string.Empty;
			}
        }

        private void ImprimirTabuleiro() 
        {
            Console.Clear();
            int linha = 0;

            for (int i = 0; i <= this.bound0; i++)
            {
                // mudou de linha?
                if (linha != i)
                {
                    linha = i;
                    Console.WriteLine("\n");
                }

                for (int j = 0; j <= this.bound1; j++)
                {
                    if (this.tabuleiro[i, j] != "_")
                        Console.Write("   {0}   ", this.tabuleiro[i, j]);
                    else
                        Console.Write(" ({0},{1}) ", i, j);
                }
            }
        }

        private bool InformarJogada() 
        {
            this.jogador = (this.jogador.ToUpper() == "X") ? "O" : "X";
            int linha, coluna;
            Console.WriteLine("\n");
            Console.Write("Informe a posicao separado por \",\" para o jogador {0}: ", this.jogador);
            string[] posicao = Console.ReadLine().Split(",");

            if (!int.TryParse(posicao[0], out linha) || !int.TryParse(posicao[1], out coluna)) {
				this.jogador = (this.jogador.ToUpper() == "X") ? "O" : "X";
				return false;
            }
            
            if (this.tabuleiro[linha, coluna] == "_")
            {
                this.tabuleiro[linha, coluna] = jogador;
                return true;
			}

            this.jogador = (this.jogador.ToUpper() == "X") ? "O" : "X";
            return false;
        }

        public void Jogar()
        {
            this.ImprimirTabuleiro();

            while (this.EmAndamento)
            {
                if (!this.InformarJogada())
                    Console.Write("Erro ao informar a jogada");
                this.ImprimirTabuleiro();
            }

            Console.WriteLine("\n");
            if (!string.IsNullOrEmpty(this.Vencedor))
                Console.WriteLine("Vencedor: {0}", this.Vencedor);
            else
                Console.WriteLine("Deu velha");
        }


    }
 
}
