﻿using System;

namespace TipoDados
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tamanho do tipo Int: {0}", sizeof(int));
            Console.WriteLine("Tamanho do tipo Long: {0}", sizeof(long));
            Console.WriteLine("Tamanho do tipo Byte: {0}", sizeof(byte));
            Console.WriteLine("Tamanho do tipo Sbyte: {0}", sizeof(sbyte));
            Console.ReadKey();
        }
    }
}
