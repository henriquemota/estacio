﻿using System;
namespace Exemplo
{
    public class Pessoa
    {
        public Pessoa() { }
      
        public Pessoa(string nome, int idade) 
        {
            this.nome = nome;
            this.idade = idade;
        }

        string nome;
        int idade;

        public string Nome { get { return this.nome; } set { this.nome = value; } }
        public int Idade { get { return this.idade; } set { this.idade = value; }}
    }
}
