﻿using System;

namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
			try
			{
				string nome;
				int idade;

    			Console.Write("Favor informar o nome: ");
				nome = Console.ReadLine();

				Console.Write("\nFavor informar a idade e observar a mágica: ");
				idade = int.Parse(Console.ReadLine());

				SayHello(nome, idade);
				Console.ReadKey();
			}
			catch (FormatException)
			{
				Console.WriteLine("Alguns dados foram informados em formato incorreto.");
			}
			catch (Exception ex)
            {
				Console.WriteLine(ex.Message);
            }
			finally
			{
				Console.WriteLine("Sempre vou falar tchau");
			}
            
        }

		static void SayHello(string nome, int idade) {

			if (string.IsNullOrEmpty(nome))
				throw new NomeException();

			Console.WriteLine("Hello {0}", nome);
		}

        
    }
}
