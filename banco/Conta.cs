using System;

class Conta {
  decimal saldo;
  public string Agencia { get; set; }
  public string Numero { get; set; }
  public decimal Limite { get; set; }
  public decimal Saldo { get { return this.saldo; } }

  public void Depositar (decimal valor) 
  { 
    this.saldo += valor;
  }
  public bool Sacar (decimal valor) 
  { 
    if (valor < this.saldo || valor < this.saldo + this.Limite )
    {
      this.saldo -= valor;
      return true;
    }
    return false;
  }

}