﻿using System;

namespace banco {
    class Program {
        static void Main (string[] args) {
            Cliente c = new Cliente ();
            Console.Write ("Informe o CPF do cliente: ");
            c.CPF = Console.ReadLine ();
            Console.Write ("Informe o Nome do cliente: ");
            c.Nome = Console.ReadLine ();
            Console.Write ("Informe a Data de nascimento do cliente: ");
            c.Nascimento = DateTime.Parse (Console.ReadLine ());

            c.Conta = new Conta ();
            Console.Write ("Informe a agência da conta do cliente: ");
            c.Conta.Agencia = Console.ReadLine ();
            Console.Write ("Informe o número da conta do cliente: ");
            c.Conta.Numero = Console.ReadLine ();
            Console.Write ("Informe o limite da conta do cliente: ");
            c.Conta.Limite = decimal.Parse (Console.ReadLine ());

            Console.Clear();
            Console.WriteLine ("O saldo atual da conta do sr(a). {0} é R$ {1}", c.Nome, c.Conta.Saldo.ToString ("n"));

            string acao = string.Empty;
            while (acao != "q") {
                Console.WriteLine ("Informe a ação desejada");
                Console.WriteLine ("D: Depositar");
                Console.WriteLine ("S: Sacar");
                Console.WriteLine ("Q: Sair");
                acao = Console.ReadLine ();
                Console.Clear();
                switch (acao) {
                    case "D":
                    case "d":
                        Console.Write ("Informe o valor a ser depositado: ");
                        c.Conta.Depositar(decimal.Parse (Console.ReadLine ()));
                        Console.WriteLine ("O saldo atual da conta do sr(a). {0} é R$ {1}", c.Nome, c.Conta.Saldo.ToString ("n"));

                        break;
                    case "S":
                    case "s":
                        Console.Write ("Informe o valor a ser sacado: ");
                        if (!c.Conta.Sacar(decimal.Parse (Console.ReadLine ())))
                            Console.WriteLine ("Operação não realizada. Cliente não possui saldo.");
                        Console.WriteLine ("O saldo atual da conta do sr(a). {0} é R$ {1}", c.Nome, c.Conta.Saldo.ToString ("n"));

                        break;
                    case "Q":
                    case "q":
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }

        }

    }
}