using System;

class Cliente 
{
  public Cliente() {}
  public Cliente(string cpf, string nome, DateTime nascimento) 
  {
    this.CPF = cpf;
    this.Nome = nome;
    this.Nascimento = nascimento;
  }

  public string CPF { get; set; }
  public string Nome { get; set; }
  public DateTime Nascimento { get; set; }
  public int Idade {  get { return (int)(DateTime.Now - this.Nascimento).TotalDays / 365; } }
  public Conta Conta { get; set; }

}