﻿using System;
using ExercicioOOHeranca.Entity;
using ExercicioOOHeranca.Enum;

namespace ExercicioOOHeranca
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal a1, a2;
            Pessoa p1, p2;

            a1 = new Baleia();
            a1.Nome = "Free Willy";

            a2 = new Morcego();
            a2.Nome = "Batman";

            p1 = new Homem { Codigo = 1, Nome = "Joao", Sexo = Sexo.Masculino };
            p2 = new Mulher { Codigo = 2, Nome = "Maria", Sexo = Sexo.Feminino };

            a2.Comer();
            a1.Dormir();
            p2.Acordar();
            p1.Comer();
            p2.Comer();
        }
    }
}
