﻿using System;
using ExercicioOOHeranca.Enum;

namespace ExercicioOOHeranca.Entity
{
    abstract class Pessoa
    {

        public int Codigo
        {
            get;
            set;
        }

        public string Nome
        {
            get;
            set;
        }

        public Sexo Sexo
        {
            get;
            set;
        }
     
        public void Acordar()
        {
            Console.WriteLine("{0} acordou", this.Nome);
        }

        public abstract void Comer();

        public void Dormir()
        {
            Console.WriteLine("{0} dormiu", this.Nome);
        }
    }
}
