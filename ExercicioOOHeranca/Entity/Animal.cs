﻿using System;
namespace ExercicioOOHeranca.Entity
{
    class Animal
    {
        public string Nome
        {
            get;
            set;
        }

        public void Acordar() 
        {
            Console.WriteLine("{0} acordou", this.Nome);
        }

        public void Dormir()
        {
            Console.WriteLine("{0} dormiu", this.Nome);
        }

        public void Comer()
        {
            Console.WriteLine("{0} comeu", this.Nome);
        }
    }


}
