﻿using System;
namespace ExercicioOOHeranca.Entity
{
    class Homem : Pessoa
    {
        public override void Comer()
        {
            Console.WriteLine("O homem {0} comeu ", this.Nome);

        }
    }
}
