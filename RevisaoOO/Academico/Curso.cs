﻿using System;
namespace RevisaoOO
{
    public class Curso
    {
		public string CodigoCurso { get; set; }
		public string Descricao { get; set; }
		public Aluno[] Alunos { get; set; }
    }
}
