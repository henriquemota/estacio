﻿using System;
namespace RevisaoOO
{
    public class Professor
    {
        
		public string CodigoProfessor { get; set; }
		public string CPF { get; set; }
		public Curso[] Cursos { get; set; }
		public Titulacao Titulacao { get; set; }

		public void Identificar()
        {
            Console.WriteLine("Professor identificado");
        }

    }
}
