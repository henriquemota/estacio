﻿using System;
namespace RevisaoOO
{
	public class Aluno : Pessoa
    {
		public string Matricula { get; set; }
		public Avaliacao[] Avaliacoes { get; set; }
		public Curso[] Cursos { get; set; }

		public void Matricular() 
		{
			Console.WriteLine("Aluno matriculado");
        }

        public void Identificar()
        {
			Console.WriteLine("Aluno identificado");
		}
    }
}
