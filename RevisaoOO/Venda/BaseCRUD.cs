﻿using System;
namespace RevisaoOO.Venda
{
	public abstract class BaseCRUD
    {
		public void Inserir() { }
        public void Excluir() { }
        public void Alterar() { }
        public void Consultar() { }
    }
}
