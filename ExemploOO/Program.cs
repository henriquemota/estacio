﻿using System;

namespace ExemploOO
{
    class Program
    {
        static void Main(string[] args)
        {
            Cliente c = new Cliente();
			Cliente d = new Cliente();

            c.CPF = "111";
            c.Nome = "Joao da Silva";
            c.Nascimento = DateTime.Parse("1974/10/24");

            d.CPF = "222";
            d.Nome = "Maria da Silva";
            d.Nascimento = DateTime.Parse("1996/10/24");

            Console.Write("A idade de {0} é {1}", c.Nome, c.Idade);
            c.Andar();

            Console.Write("A idade de {0} é {1}", d.Nome, d.Idade);
            d.Andar();
            d.Correr();

            Console.ReadKey();
        }
    }
}
