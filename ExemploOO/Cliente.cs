﻿using System;
namespace ExemploOO
{
    public class Cliente
    {
        public string Nome
        {
            get;
            set;
        }

        public string CPF
        {
            get;
            set;
        }

        public DateTime Nascimento
        {
            get;
            set;
        }

        public int Idade
        {
            get { return (int)((DateTime.UtcNow - this.Nascimento).TotalDays / 365); }        
        }

        public void Andar()
        {
            Console.WriteLine("{0} andando", this.Nome);
            Console.ReadKey();
        }

        public void Correr()
        {
            Console.WriteLine("{0} correndo", this.Nome);
            Console.ReadKey();
        }
    }
}
