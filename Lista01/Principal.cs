﻿using System;

namespace Lista01
{
    class Princial
    {
        static void Main(string[] args)
        {
            ContaFisica a = new ContaFisica();
            a.Creditar(100);
            Console.WriteLine("Saldo da conta {0}", a.Saldo);

            ContaJuridica b = new ContaJuridica();
            b.Creditar(100);
            Console.WriteLine("Saldo da conta {0}", b.Saldo);

            Console.ReadKey();
        }
    }

}
