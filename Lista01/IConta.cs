﻿using System;
namespace Lista01
{
    interface IConta
    {
        void Debitar(decimal valor);
        void Creditar(decimal valor);
        void Estornar(decimal valor);
    }
}
