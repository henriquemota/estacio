﻿using System;
namespace Lista01
{
    abstract class Conta : IConta
    {

        protected decimal saldo;

        public string Numero
        {
            get;
            set;
        }

        public string Agencia
        {
            get;
            set;
      
        }

        public string Digito
        {
            get;
            set;
        }

        public decimal Saldo
        {
            get { return this.saldo; }
        }

        public virtual void Creditar(decimal valor)
        {
            this.saldo += valor;
        }

        public virtual void Debitar(decimal valor)
        {
            if (valor < this.saldo)
                this.saldo -= valor;
        }

        public virtual void Estornar(decimal valor)
        {
            this.saldo += valor;
        }
    }
}
