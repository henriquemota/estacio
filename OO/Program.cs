﻿using System;

namespace OO
{
    class Program
    {
        static void Main(string[] args)
        {
            Pessoa[] pessoas = { 
                new Pessoa { Nome = "Joao", Sexo = "M" }, 
                new Pessoa { Nome = "Pedro", Sexo = "M" }, 
                new Pessoa { Nome = "Maria", Sexo = "F" }, 
                new Pessoa { Nome = "Jose", Sexo = "M" }, 
                };
            

            foreach (Pessoa item in pessoas)
            {
                Console.WriteLine("{0}", item.Nome);
                foreach (char c in item.Nome)
					Console.WriteLine("{0} - ", c);

            }


        }
    }

    class Pessoa
    {
        public string Nome { get; set; }
        public string Sexo { get; set; }
    }
}
