﻿using System;

namespace Boxing
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 10;
            object o = i;
            int j = (int)o;
        }
    }
}
