﻿using System;
namespace Exercicios.Lista01
{
    public class Ex05
    {
        /*
            Dados n e uma sequência de n números inteiros, determinar a soma dos números pares. 
        */

        public static void Executa()
        {
            Console.Clear();
            Console.WriteLine("Informe o total de números da sequência");
            byte num = Convert.ToByte(Console.ReadLine());
            int valor = 0, soma = 0;
            for (int i = 0; i < num; i++)
            {
                Console.WriteLine("Informe o valor do numero de posição {0}", i + 1);

                valor = Convert.ToInt32(Console.ReadLine());
                if (valor % 2 == 0)
                    soma += valor;
            }

            Console.WriteLine("A soma dos {0} pares e {1}", num, soma);
            Console.WriteLine("Pressione enter para continuar");
            Console.ReadKey();
        }
    }
}
