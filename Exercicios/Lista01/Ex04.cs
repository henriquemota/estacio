﻿using System;
namespace Exercicios.Lista01
{
    public class Ex04
    {
        /*
            Uma loja de discos anota diariamente durante o mês de março a quantidade de discos vendidos. 
            Determinar em que dia desse mês ocorreu a maior venda e qual foi a quantidade de discos vendida nesse dia.
        */

        public static void Executa()
        {
            Console.Clear();
            Console.WriteLine("Informe o total de dias do mês");
            byte dias = Convert.ToByte(Console.ReadLine());
            int[] dados = new int[dias];

            int maior = 0;
            string r = string.Empty;

            // entrada de dados
            for (int i = 0; i < dados.Length; i++)
            {
                Console.WriteLine("Informe a quantidade para o dia {0}", i + 1);
                dados[i] = Convert.ToInt32(Console.ReadLine());

                if (maior <= dados[i])
                {
                    maior = dados[i];
					//r += string.Format("dia {0} ;", i+1);
                }
            }

            Console.WriteLine("Os dias de maior valor foram: {0} - valor: {1}", r, maior);
            Console.WriteLine("Pressione enter para continuar");
            Console.ReadKey();
        }
    }
}
