﻿using System;
namespace Exercicios.Lista01
{
    public class Ex02
    {
        /*
            Dado um número inteiro positivo n, imprimir os n primeiros naturais impares.
            Exemplo: Para n=4 a saída deverá ser 1,3,5,7.
        */

        public static void Executa()
        {
            Console.Clear();
            Console.WriteLine("Informe a quantidade de números");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Os {0} números impares são {1}", n, ExibeImpares(n));
            Console.WriteLine("Pressione enter para continuar");
            Console.ReadKey();
        }

        static string ExibeImpares(int n)
        {
            int total = 0, i = 0;
            string r = string.Empty;
            while (total < n)
            {
                if (i > 0 && i % 2 != 0)
                {
                    r += string.Format("{0}, ", i);
                    total++;
                }
				i++;

            }
            return r.Substring(0, r.Length - 2);
        }
    }
}
