﻿using System;
namespace Exercicios.Lista01
{
    public class Ex03
    {
        /*
            Dados um inteiro x e um inteiro não-negativo n, calcular x n. 
        */

        public static void Executa()
        {
            Console.Clear();
            int x, n;
            Console.WriteLine("Informe o inteiro X");
            x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Informe o inteiro não negativo N");
            n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("A multiplicação de {0} por {1} é {2}", x, n, x*n);
            Console.WriteLine("Pressione enter para continuar");
            Console.ReadKey();
        }


    }
}
