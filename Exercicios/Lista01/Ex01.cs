﻿using System;
namespace Exercicios.Lista01
{
    public class Ex01
    {
        /*
            Dado um número inteiro positivo n, calcular a soma dos n primeiros números inteiros positivos.
		*/

        public static void Executa()
        {
            Console.Clear();
            Console.WriteLine("Informe o valor de \"n\"");
            int n = Convert.ToInt32(Console.ReadLine());
            //int n = int.Parse(Console.ReadLine());
            Console.WriteLine("A soma dos \"n\" primeiros números pares é {0}", SomaN(n));
            Console.WriteLine("Pressione qualquer tecla para continuar");
            Console.ReadKey();
        }

        static int SomaN(int n) 
        {
            int r = 0;
            for (int i = 0; i <= n; i++)
            {
                if (i % 2 == 0)
                    r += i;
                    
            }
            return r;
        }
		
    }
}
