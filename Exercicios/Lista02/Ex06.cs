﻿using System;
namespace Exercicios.Lista02
{
    public class Ex06
    {

        /*
            Escreva um algoritmo que leia uma certa quantidade de números e em 
            seguida leia os números. Imprima o maior deles e quantas vezes o maior 
            número foi lido. A quantidade de números a serem lidos deve ser fornecida 
            pelo usuário. Assuma que o usuário sempre fornecerá um número positivo. 
        */

        public static void Executa()
        {
            Console.Clear();
            Console.WriteLine("Informe o total de números gerados");
            int[] n = new int[int.Parse(Console.ReadLine())];

            Random r = new Random();

            // popular o array
            for (int i = 0; i < n.Length; i++)
            {
                n[i] = r.Next(1, 1000);
            }

            // identificar o maior de todos


            Console.WriteLine("Pressione enter para continuar");
            Console.ReadKey();

        }
    }
}
