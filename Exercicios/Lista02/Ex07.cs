﻿using System;
namespace Exercicios.Lista02
{
    public class Ex07
    {
		const decimal horaNormal = (decimal)10.0, horaExtra = (decimal)15.0;

        public static void Executa()
        {
            Console.Clear();

            decimal totalNormal = 0, totalExtra = 0;

            Console.Write("Informe o total de horas normais: ");
            totalNormal = decimal.Parse(Console.ReadLine());

            Console.Write("Informe o total de horas extra: ");
            totalExtra = decimal.Parse(Console.ReadLine());

            Console.WriteLine("O salário anual do trabalhador foi de {0}", CalcularSalario(totalNormal, totalExtra));

            Console.WriteLine("Pressione enter para continuar");
            Console.ReadKey();
        }

        static decimal CalcularSalario(decimal totalNormal, decimal totalExtra)
        {
            decimal inss = 5, ir = 18;
            decimal salario = (totalNormal * horaNormal) + (totalExtra * horaExtra);
            inss = salario * inss / 100;
            ir = salario * ir / 100;
            return salario - inss - ir;
        }
    }
}
