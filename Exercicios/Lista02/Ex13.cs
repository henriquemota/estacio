﻿using System;
namespace Exercicios.Lista02
{
    public class Ex13
    {
		const decimal salario = 500, comissao = 50, percentualVenda = 5;

        public static void Executa()
        {
            Console.Clear();

            string nomeVendedor = string.Empty;
            int numeroVendas = 0;
            decimal valorVendas = 0;

            Console.Write("Informe o nome do vendedor: ");
            nomeVendedor = Console.ReadLine();

            Console.Write("Informe o total de de vendas do vendedor {0}: ", nomeVendedor);
            numeroVendas = int.Parse(Console.ReadLine());

            Console.Write("Informe o valor total de vendido no mês: ");
            valorVendas = decimal.Parse(Console.ReadLine());

            Console.WriteLine("O salário do {0} ficou em R$ {1}", nomeVendedor, CalcularSalario(numeroVendas, valorVendas));

            Console.WriteLine("Pressione enter para continuar");
            Console.ReadKey();
        }

        static decimal CalcularSalario(int totalVendas, decimal valorTotalVendido)
        {
            return salario + (totalVendas * comissao) + (valorTotalVendido * percentualVenda / 100); ;
        }
    }
}
