﻿using System;

namespace Exercicios
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }

        static void Menu()
        {
            string menu = string.Empty;
            Console.WriteLine("Informe a opção desejada:");
            Console.WriteLine("1.1: Lista 1 ex 1");
            Console.WriteLine("1.2: Lista 1 ex 2");
            Console.WriteLine("1.3: Lista 1 ex 3");
            Console.WriteLine("1.4: Lista 1 ex 4");
            Console.WriteLine("1.5: Lista 1 ex 5");
            Console.WriteLine("2.7: Lista 2 ex 7");
            Console.WriteLine("2.13: Lista 2 ex 13");
            Console.WriteLine("Q: Sair");

            menu = Console.ReadLine();

            switch (menu)
            {
                case "q":
                case "Q":
                    return;
                case "1.1":
                    Lista01.Ex01.Executa();
                    break;
                case "1.2":
                    Lista01.Ex02.Executa();
                    break;
                case "1.3":
                    Lista01.Ex03.Executa();
                    break;
                case "1.4":
                    Lista01.Ex04.Executa();
                    break;
                case "1.5":
                    Lista01.Ex05.Executa();
                    break;
                case "2.7":
                    Lista02.Ex07.Executa();
                    break;
                case "2.13":
                    Lista02.Ex13.Executa();
                    break;
            }
            Console.Clear();
			Menu();

        }

    }
}
